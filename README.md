PublicWBB Component
===================

This component is just the receiver for the end of day commits made by the distributed PrivateWBB. It does not provide a way of accessing the received data, only a way of combining the commit messages from each peer into a single, jointly signed, commitment.

Running
=======

PublicWBB starts the Public WBB receiver service. This listens for connections from peers and processes their daily commit packages.  

Usage:  
PublicWBB \[pathToConfigFile\]  
pathToConfigFile: path to configuration file to use.

